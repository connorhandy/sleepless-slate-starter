window.slate = window.slate || {};
window.theme = window.theme || {};

/*================ Slate/Shopify ================*/
// =require slate/a11y.js
// =require slate/customers-addresses.js
// =require slate/customers-login.js

/*================ MODULES ================*/
// =require modules/product-variants.js

$(document).ready(function() {

  // Common a11y fixes
  slate.a11y.pageLinkFocus($(window.location.hash));

  $('.in-page-link').on('click', function(evt) {
    slate.a11y.pageLinkFocus($(evt.currentTarget.hash));
  });
});
