var productVariants = (function () {
	var productSelect = $("#product-variants-select"),
		variants = productSelect.children('option'),
		priceField = document.getElementById('price-field'),
		availableVariants = [],
		optionSelects = $(".option-wrapper-select"),
		submitBtn = document.getElementById('product-submit');

	if (optionSelects.length) optionSelects.change(updateVariants);

	if (productSelect.length) {
		//loop through hidden variants and turn them into usable objects
		for (var i = 0; i < variants.length; i++) {
			var options = variants[i].getAttribute('data-options').split(",");
			for (var k = 0; k < options.length; k++) {
				options[k] = decodeURIComponent(options[k]);
			}
			var newVariant = { "options": options, "id": variants[i].getAttribute('value'), "price": variants[i].getAttribute('data-price') }
			availableVariants.push(newVariant);
		}
		if (optionSelects.length) updateVariants();
		if (variants.length == 0) {
			setSubmitButton(true);
		}
	}

	function updateVariants() {
		var currentOptions = getCurrentOptions(),
			matchedVariant = null,
			inStock = false;
		//loop through all variants
		for (var i = 0; i < availableVariants.length; i++) {
			var variantOptions = availableVariants[i].options,
				count = 0;
			//loop through options for current variant
			for (var j = 0; j < variantOptions.length; j++) {
				//loop through selected options and check if they match with variantOptions
				var selectedOptions = currentOptions;
				for (var k = 0; k < currentOptions.length; k++) {
					if (variantOptions[j] === selectedOptions[k]) {
						count++;
					}
				}
			}
			//if count equals options length that means the variant is available
			if (count === variantOptions.length) {
				inStock = true;
				matchedVariant = availableVariants[i];
				priceField.innerHTML = matchedVariant.price;
				break;
			}
		}
		if (inStock) {
			//change selected variant in official product select
			for (var i = 0; i < variants.length; i++) {
				if (variants[i].getAttribute('value') === matchedVariant.id) {
					variants[i].setAttribute('selected', 'selected');
				} else variants[i].removeAttribute('selected');
			}
			setSubmitButton(false);
		} else {
			setSubmitButton(true);
		}
	}

	function setSubmitButton(disable) {
		if (!submitBtn) return;
		if (disable) {
			submitBtn.setAttribute('disabled', 'disabled');
			$(submitBtn).hide();
			$(submitBtn).children('span').text('SOLD OUT');
			$(".contact-button").show();
		} else {
			submitBtn.removeAttribute('disabled');
			$(submitBtn).children('span').text('ADD TO CART');
		}
	}

	function getCurrentOptions() {
		//get array of currently selected options
		var options = [];
		for (var i = 0; i < optionSelects.length; i++) {
			options.push(decodeURIComponent(optionSelects[i].value));
		}
		return options;
	}
})();