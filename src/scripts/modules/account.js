//forgot password form
var forgotPassword = (function() {
	var forgotBtn = $("#forgot-password"),
		loginForm = $("#login-form"),
		forgotForm = $("#forgot-password-form"),
		sentEmail = $("#successful-post");

	if(forgotBtn.length) forgotBtn.click(function() {
		loginForm.hide();
		forgotForm.show();
		$("#login-header").text('RECOVER PASSWORD');
	});

	if(sentEmail.length) $("#form-reset-success").html("<p><em>Reset Password Email Sent!</em></p>");
})();

//open/closing add/edit addresses
var accountPage = (function() {
	var addBtn = $("#add-address"),
		cancelBtn = $("#cancel-address-form"),
		addressForm = $("#address-form"),
		addressErrors = $("#address-form-errors"),
		editAddressBtn = $(".edit-address-link"),
		cancelEditBtn = $(".cancel-edit-address");

	if(editAddressBtn.length) editAddressBtn.click(editAddress);
	if(addressErrors.length) addressForm.slideDown();

	if(cancelBtn.length) cancelBtn.click(function() {
		addressForm.slideUp();
	});

	if(addBtn.length) addBtn.click(function() {
		addressForm.slideDown();
	});

	if(cancelEditBtn.length) cancelEditBtn.click(function() {
		$(this).parents('.edit-address').slideUp();
	});

	function editAddress() {
		$(this).parents('.customer-address').find('.edit-address').slideDown();
	}
})();	