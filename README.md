For official slate documentation go here https://www.npmjs.com/package/@shopify/slate

Getting Started:

- Install slate via npm if you haven't already, on the command line type: npm install -g @shopify/slate
- Now you need to link up this theme to the shopify store, in the root of this directory go to config.yml, instructions are in there
- With config.yml set up. In the command line, type: slate start (must be in this directory)
- Slate will now upload your assets to the theme you got the theme id from, you can work from localhost:3000 with hot loading or from previewing the theme.
- Now you can preview the theme or publish it on the shopify site! Yay. I'd rename it too.
- From now on when you start working on this theme, in this directory in the command line type: slate watch, when you do that anytime you save the changes will be uploaded to the shopify store server.
- Follow the link at the top for any more info on slate. Or ask me!

Getting Around:

- This themes templates are purposely kept mostly blank, except for the account pages. 
- Account templates are based on the standard helgy theme and css is in styles/templates/account.scss 
- I've included bootstrap (just the grid)/font awesome/normalize.css, slate includes a lot of js files, you can delete them or remove them from theme.js
- Include all js files in theme.js, vendor.js is for frameworks/libraries. Do not remove these files.
- Sass is compiled in styles/theme.scss

Deploying for Production:

I've edited the slate npm package so that if you run 'slate build' in the command line it will minify all css and js assets. Running 'slate watch' won't. So before production run 'slate build'

Connor 5/4/17
